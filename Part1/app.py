import math
from flask import Flask

app = Flask(__name__)

@app.route('/numericalintegralservice/<a>/<b>')
def integration(a, b):
  result = {}
  for i in range(1,7):
    N = 10**i
    dx = (float(b) - float(a))/N
    integral = 0
    for i in range(N):
      I = abs(math.sin(float(a) + dx*(i+0.5)))*dx
      integral += I
    result[N] = integral
  return result
