import time 
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def numericalintegralservice(self):
        self.client.get("/api/HttpTrigger8?b=0&a=3.14159")