import logging

import azure.functions as func
import math

def integration(a, b):
  result = {}
  for i in range(1,7):
    N = 10**i
    dx = (float(b) - float(a))/N
    integral = 0
    for i in range(N):
      I = abs(math.sin(float(a) + dx*(i+0.5)))*dx
      integral += I
    result[N] = integral
  return result

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    
    a = req.params.get('a')
    b = req.params.get('b')
    


    if a and b:
        integral = integration(float(a), float(b))
        return func.HttpResponse(f"Hello, The integral is.{integral}")
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
             status_code=200
        )
